# -*- coding: utf-8 -*-
"""
Created on Sun May 7 09:27:36 2023

@author: Siddharth
"""

import os
import pandas as pd
import sqlite3
import inspect

def getFolderData(path):

    files = os.listdir(path)
    completePaths = [os.path.join(path, f) for f in files]
    return completePaths

def write2file(data, to_csv=False, to_db=False):
    """
    Function to write the compiled data to a file that can be transferred or
    reused next time without the need to scan all over again. Data can be 
    stored as csv or db.

    Parameters
    ----------
    data : list of File objects
        Contains information about all the files stored as File object.
    to_csv : bool, optional
        Switch to write to .csv file. The default is False.
    to_db : bool, optional
        Switch to write to .db file. The default is False.

    Returns
    -------
    None.

    """
    
    if to_csv:
        names = [d.name for d in data]
        size = [d.size for d in data]
        ext = [d.ext for d in data]
        path = [d.path for d in data]

        inputData = {
            'Name' : names,
            'Size' : size,
            'Type' : ext,
            'Location' : path
        }

        df = pd.DataFrame(inputData)
        csvPath = os.path.join(os.getcwd(), 'fileList.csv')
        df.to_csv(path_or_buf=csvPath)
        print(f'\nData successfully written to {csvPath}')

    if to_db:
        connection = sqlite3.connect('fileList.db')
        cursor = connection.cursor()
        
        generalData = [(d.name, d.size, d.ext, d.path) for d in data]
        
        cursor.execute("CREATE TABLE generalList (Name text, Size text, \
                       Type text, Location text)")
            
        cursor.executemany("INSERT INTO generalList values (?, ?, ?, ?)", \
                           generalData)
        connection.commit()
        
        connection.close()
        
        
def read_db(database):
    """
    Function to read and display data from database file.

    Parameters
    ----------
    database : str
        Link to the database file.

    Returns
    -------
    None.

    """
    connection = sqlite3.connect(database)
    cursor = connection.cursor()
    
    cursor.execute("SELECT * FROM imageList")
    sizeSearch = cursor.fetchall()
    print(sizeSearch)
    
    connection.close()
    
    
def writeData2File(data, output, name, to_csv=False, to_db=False):
    """
    Function to write the compiled data to a file that can be transferred or
    reused next time without the need to scan all over again. Data can be 
    stored as csv or db.

    Parameters
    ----------
    data : list of File objects
        Contains information about all the files stored as File object.
    output : string/bool, optional
        Path to the directory where output must be stored.
    name : string
        Desired name of the output
    to_csv : bool, optional
        Switch to write to .csv file. The default is False.
    to_db : bool, optional
        Switch to write to .db file. The default is False.

    Returns
    -------
    None.

    """
    if to_csv:

        for att, val in data[0].__dict__.items():
            print(att, ': ', val)
        '''
        names = [d.name for d in data]
        size = [d.size for d in data]
        ext = [d.ext for d in data]
        path = [d.path for d in data]

        inputData = {
            'Name' : names,
            'Size' : size,
            'Type' : ext,
            'Location' : path
        }

        df = pd.DataFrame(inputData)
        name += '.csv'
        csvPath = os.path.join(os.getcwd(), name)
        df.to_csv(path_or_buf=csvPath)
        print(f'\nData successfully written to {csvPath}') '''

    if to_db:
        name = name + '.db'
        connection = sqlite3.connect(name)
        cursor = connection.cursor()
        
        generalData = [(d.name, d.size, d.ext, d.path) for d in data]
        
        cursor.execute(f"CREATE TABLE imageList (Name text, Size text, \
                       Type text, Location text)")
            
        cursor.executemany("INSERT INTO imageList values (?, ?, ?, ?)", \
                           generalData)
        connection.commit()
        
        connection.close()
    