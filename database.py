# -*- coding: utf-8 -*-
"""
Created on Wed May 24 09:26:40 2023

@author: Siddharth
"""
import sqlite3
from file_data import File
from image_data import ImageData
from video_data import VideoData
import os
from tqdm import tqdm
from scanner import folder_lists
from constants import IMAGES, FIELD_TYPES, VIDEOS
import inspect

class Database():
    
    def __init__(self, database_file=None, data=None):
        
        self.db_name = 'fileDatabase.db'
        self.db = database_file
        if self.db_name in os.listdir():
            self.db = self.db_name
        # data = self.checkData(data)
        # self.run_command()
        if not self.checkDatabase():
            self.db = 'fileDatabase.db'
            self.db = os.path.join(os.getcwd(), self.db)
            tableName = 'generalFileList'
            fileObjects = [File(path=p) for p in data]
            self.createTable(tableName, fileObjects)
            self.insertData(tableName, fileObjects)
        else:
            tables = self.listTables()
            fileObjects = [File(path=p) for p in data]
            self.updateTable(tables[0], fileObjects)
            # newTables = list(filter(lambda x:x not in tables, data))
            # if len(newTables) > 0:
            #    addNewTable = input('New tables found. Add them? : ')
            #    if addNewTable.upper() == 'Y':
            #        for key in newTables:
            #            self.createTable(key, data[key])

        self.createImageTable(fileObjects)
        self.createVideoTable(fileObjects)
        
    def checkDatabase(self):
        if not self.db:
            print("No database found. Creating fresh database with discovered files.")
            return False
        if not (os.path.splitext(os.path.split(self.db)[1])[1] == '.db' and os.path.exists(self.db)):
            print("No database file found. Creating fresh database.")
            return False
        else:
            return True        
        

    def renameCols(self, tableName, oldName, newName):
        connection = sqlite3.connect(self.db)
        cursor = connection.cursor()
        query = f'ALTER TABLE {tableName} RENAME COLUMN {oldName} TO {newName}'
        cursor.execute(query)
        connection.commit()
        connection.close()


    def cleanColNames(self, data):
        names = self.getFieldNames(data)
        names = names.split(',')
        names = [f.split(' ') for f in names]
        names = [j for i in names for j in i if j not in FIELD_TYPES]
        return names


    def checkData(self, data):
        '''
        print(f"Following data types were found: {data.keys()}")
        removeData = input('Enter the data to remove, press Enter to continue without deleting anything: ')
        while removeData:
            if removeData in data:
                val = data.pop(removeData, None)
                removeData = input('Data removed. Enter data type name to remove more data: ')
            else:
                print('Data type not found. Possible typing error. Try again! ')
                removeData = input('Enter data type. Press enter to skip: ')
        '''
        return data



    def createTable(self, tableName, data):
        connection = sqlite3.connect(self.db)
        cursor = connection.cursor()
        fieldNames = self.getFieldNames(data)
        query = f"CREATE TABLE {tableName} ({fieldNames}, CONSTRAINT uniquePath UNIQUE (path))"
        cursor.execute(query)
        # query = f"ALTER TABLE {tableName} ADD CONSTRAINT uniquePath UNIQUE (path)"
        # cursor.execute(query)
        connection.commit()
        connection.close()

        
    def listColumnNames(self, tableName):
        connection = sqlite3.connect(self.db)
        cursor = connection.cursor()
        fieldNames = cursor.execute(f'SELECT * FROM {tableName}')
        fieldNames = [description[0] for description in cursor.description]
        connection.close()
        return fieldNames
    

        
    def listTables(self):
        connection = sqlite3.connect(self.db)
        cursor = connection.cursor()
        cursor.execute(f'SELECT * FROM sqlite_master WHERE type="table"')
        tables = cursor.fetchall()
        t = [tab[1] for tab in tables]
        connection.close()
        return t
        

    def updateTable(self, tableName, dataList):
        connection = sqlite3.connect(self.db)
        cursor = connection.cursor()
        attributeNames = [a.lower() for a in self.listColumnNames(tableName)]
        data2Push = []
        for dataRow in dataList:

            row = [getattr(dataRow, att) for att in attributeNames]
            row = tuple(row)
            data2Push.append(row)

        questionMarks = ', '.join(['?' for l in attributeNames])
        query = f"INSERT INTO {tableName} VALUES ({questionMarks})"
        for row in data2Push:
            try:
                cursor.execute(query, row)
            except sqlite3.IntegrityError:
                j = 0

        connection.commit()
        connection.close()
        print(f"Table: {tableName} updated")



    def insertData(self, tableName, data):
        names = self.cleanColNames(data)
        colsNotFound = list(set(self.listColumnNames(tableName)) - set(names))
        if len(colsNotFound) > 0:
            raise RuntimeError (f'Following columns were not found: {", ".join(colsNotFound)}')
        '''print(f"Following new columns were found in the dataset. Add them? ")
        newCols = list(set(names) - set(self.listColumnNames(table)))
        for n in newCols:
            print(f'{n}')'''
        if len(self.listColumnNames(tableName)) == 0:
            return
        
        attributeNames = [a.lower() for a in self.listColumnNames(tableName)]
        connection = sqlite3.connect(self.db)
        cursor = connection.cursor()
        dataToPush = []
        for obj in data:
        
            row = [getattr(obj, att) for att in attributeNames]
            row = tuple(row)
            dataToPush.append(row)
        
        questionMarks = ', '.join(['?' for l in attributeNames])
        cursor.executemany(f"INSERT INTO {tableName} VALUES ({questionMarks})", dataToPush)
            
        connection.commit()
        connection.close()


        
    def getFieldNames(self, data):

        attributes = inspect.getmembers(data[0], lambda a:not(inspect.isroutine(a)))
        fieldValues = [a for a in attributes if not(a[0].startswith('__') and a[0].endswith('__'))]
        fieldNames = [[b[0], type(b[1]).__name__] for b in fieldValues]
        
        fieldNames = [list(map(lambda x: x.replace('str', 'text'), l)) for l in fieldNames]

        # Join individual field names with their type
        fieldNames = [' '.join(element) for element in fieldNames]

        # Convert field name to sentence case
        fieldNames = [element[0].upper()+element[1:] for element in fieldNames]

        # Join entire list into one string to be passed as SQL query
        fieldNames = ', '.join(fieldNames)

        return fieldNames
    

    def createImageTable(self, fileObjects):
        connection = sqlite3.connect(self.db)
        cursor = connection.cursor()
        tables = self.listTables()
        tableName = 'imageTable'
        imageData = [ImageData(row.path) for row in fileObjects if row.ext in IMAGES]
        fieldNames = self.getFieldNames(imageData)
        placeHolders = ['?' for i in range(len(fieldNames.split(",")))]
        
        if not tableName in tables:
            query = f"CREATE TABLE {tableName} ({fieldNames}, CONSTRAINT uniquePath UNIQUE (path))"
            cursor.execute(query)
        
        self.updateTable(tableName, imageData)

    
    def createVideoTable(self, fileObjects):
        connection = sqlite3.connect(self.db)
        cursor = connection.cursor()
        tables = self.listTables()
        tableName = 'videoTable'
        imageData = [VideoData(row.path) for row in fileObjects if row.ext in VIDEOS]
        fieldNames = self.getFieldNames(imageData)
        placeHolders = ['?' for i in range(len(fieldNames.split(",")))]
        
        if not tableName in tables:
            query = f"CREATE TABLE {tableName} ({fieldNames}, CONSTRAINT uniquePath UNIQUE (path))"
            cursor.execute(query)
        
        self.updateTable(tableName, imageData)
            


    def runQuery(self, query):
        connection = sqlite3.connect(self.db)
        cursor = connection.cursor()
        cursor.execute(query)
        connection.commit()
        connection.close()

if __name__=='__main__':
    location = 'D:\Images\Academy'
    filelist, folders, files = [], [], []
    filelist.extend([f for f in os.listdir(location) if '$' not in f])
    filelist = sorted(filelist) 
    folders.extend([os.path.join(location, f) for f in filelist \
                    if os.path.isdir(os.path.join(location, f))])
    files.extend([os.path.join(location, f) for f in filelist \
                  if os.path.isfile(os.path.join(location, f))])
        
    print("Enumerating file lists recursively")
    for f in tqdm(folders):
       fil, _ = folder_lists(f)
       files.extend(fil)
       
    print("Generating file objects")
    file_object = [File(f) for f in tqdm(files)]
    img_object = [ImageData(f.path) for f in tqdm(file_object) if f.ext in IMAGES]
    data = { 'generalList' : file_object,
            'imageList' : img_object}
    
    database = Database('List.db', data)
    database.listTables()
       
