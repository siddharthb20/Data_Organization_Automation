from PySide6.QtWidgets import QWidget, QPushButton, QFileDialog, QVBoxLayout, QHBoxLayout
from PySide6.QtWidgets import QLineEdit, QLabel, QMessageBox, QMenuBar
from PySide6 import QtWidgets
from PySide6.QtGui import QAction
import os
import pandas as pd
import sqlite3
from tqdm import tqdm
from compileData import scan
from database import Database


class Widget(QWidget):

    def __init__(self):
        super().__init__()
        self.folder = []
        self.database = None
        self.setWindowTitle("File scanner")
        self.mainWindow()
        

    def mainWindow(self):
        menuBar = QMenuBar()
        mainLayout = QVBoxLayout()
        button1 = QPushButton("Add folder")
        button2 = QPushButton("Add more folders")
        # button3 = QPushButton("Select output location")
        fileMenu = menuBar.addMenu("File")
        saveAs = QAction('Save As', self)
        fileMenu.addAction(saveAs)
        button4 = QPushButton("Compile data")
        textbox1 = QLabel()
        self.textbox2 = QLabel()
        textbox1.setText("Directories to be scanned\n")
        
        layout_v = QVBoxLayout()
        layout_v.addWidget(button1)
        layout_v.addWidget(button2)
        # layout_v.addWidget(button3)
        layout_v.addWidget(button4)
        layout_h = QHBoxLayout()
        layout_v2 = QVBoxLayout()
        layout_v2.addWidget(textbox1)
        layout_v2.addWidget(self.textbox2)
        layout_h.addLayout(layout_v2)
        layout_h.addLayout(layout_v)
        mainLayout.addWidget(menuBar)
        mainLayout.addLayout(layout_h)
        self.setLayout(mainLayout)

        button1.clicked.connect(self.addFolder)
        button2.clicked.connect(self.addMoreFolders)
        # button3.clicked.connect(self.openOutputDialog)
        button4.clicked.connect(self.runScanner)
        saveAs.triggered.connect(self.saveAs)

    def openFileDialog(self):
        dialog = QFileDialog()
        dialog.setFileMode(QFileDialog.Directory)      
        dialog.setOption(QFileDialog.ShowDirsOnly)

        dialog.setOption(QFileDialog.DontUseNativeDialog, False)
        dialog.setOption(QFileDialog.ReadOnly, False)
        dialog.setOption(QFileDialog.DontResolveSymlinks, True)

        dialog.setDirectory("C:/")

        dialog.setWindowTitle("Select folder")
        return dialog
      

    def saveAs(self):
        dialog = QFileDialog()
        dialog.setFileMode(QFileDialog.Directory)      
        dialog.setOption(QFileDialog.ShowDirsOnly)

        dialog.setOption(QFileDialog.DontUseNativeDialog, False)
        dialog.setOption(QFileDialog.ReadOnly, False)
        dialog.setOption(QFileDialog.DontResolveSymlinks, True)

        # dialog.setDirectory("C:/")

        dialog.setWindowTitle("Select folder")

        self.output = dialog.getExistingDirectory()
        if 'fileDatabase.db' in os.listdir():
            connection = sqlite3.connect('fileDatabase.db')
            cursor = connection.cursor()
            query = 'SELECT * FROM sqlite_master WHERE type="table"'
            cursor.execute(query)
            data = cursor.fetchall()
            sheetNames = [f[1] for f in data]
            
            with pd.ExcelWriter('fileList.xlsx', engine='xlsxwriter') as writer:
                for sheet in tqdm(sheetNames):
                    query = f"SELECT * FROM {sheet}"
                    df = pd.read_sql_query(query, connection)
                    df.to_excel(excel_writer=writer, sheet_name=sheet, index=False)
            connection.close()


    def runScanner(self):

        folders, files = scan(self.folder)
        self.database = Database(data=files)


    def addFolder(self):
        if len(self.folder) != 0:
            itemList = f'Non-empty list found. The list contains the following items:\n'
            for i in self.folder:
                itemList = itemList + i + '\n'
            itemList += 'Clicking "Ok" will replace these items. To add more items to this list, click "Cancel" and click on "Add more folders" in the main menu.'
            msgBox = QMessageBox.warning(self, 'Non-empty list warning',
                                         itemList, QMessageBox.Ok | QMessageBox.Cancel)

            if msgBox == QMessageBox.Ok:
                dialog = self.openFileDialog()
                self.folder = [''.join(str(dialog.getExistingDirectory()))]
            else:
                return
        
        else:
            dialog = self.openFileDialog()
            self.folder.append(dialog.getExistingDirectory())
            self.printFolders()

    def addMoreFolders(self):

        dialog = self.openFileDialog()
        self.folder.append(dialog.getExistingDirectory())
        self.printFolders()

    def printFolders(self):

        folderString = '\n'.join(self.folder)
        self.textbox2.setText(folderString)
        
