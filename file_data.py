# -*- coding: utf-8 -*-
"""
Created on Sun May 7 08:17:21 2023

@author: Siddharth
"""

import os
import time

class File():
    
    def __init__(self, path) -> None:
        
        self.name, self.ext = 0, 0
        self.size, self.size_bytes = 0, 0
        self.path = path
        self.compute_size()
        self.name, self.ext = self.extract_name()
        self.creation_time = time.ctime(os.path.getctime(path))
        self.drive = os.path.splitdrive(self.path)[0]

    def compute_size(self, show_size=False):
        """
        Method to compute the size of a file and store as a human-readable 
        attribute.

        Parameters
        ----------
        show_size : bool, optional
            Print the size to console. The default is False.

        Returns
        -------
        None.

        """    
        try:
            fileSize = os.path.getsize(self.path)
            self.size_bytes = fileSize
            prefixes = ['', 'k', 'M', 'G', 'T']
            self.size = ''
            size_of = fileSize
            for p in prefixes:
                if size_of < 1024:
                    self.size = str(round(size_of)) + ' ' + p + 'B'
                    break
                size_of /= 1024
            if show_size:
                print(f"Size of file at {self.path}: {self.size}")
        
        except OSError: 
            print(f"OSError occured for file {self.path}. Skipping...")
            self.size = 'OSError'
            self.size_bytes = 'OSError'
        

    def extract_name(self):
        """
        Method to separate out the name and extension of the file from the path

        Returns
        -------
        name : str
            Name of the file.
        ext : str
            Extension of the file.

        """
        name = os.path.split(self.path)[1]
        ext = os.path.splitext(name)[1]
        name = os.path.splitext(name)[0]
        return name, ext

    def show_attributes(self):
        """
        Method to convert the attributes to dictionary and display to console.

        Returns
        -------
        None.

        """
        file_attributes = {}
        file_attributes["Name"] = self.name
        file_attributes["Size"] = self.size
        file_attributes["Type"] = self.ext
        file_attributes["Location"] = self.path
        file_attributes["Drive"] = self.drive
