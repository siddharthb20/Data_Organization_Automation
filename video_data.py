# -*- coding: utf-8 -*-
"""
Created on Sun May 21 08:20:59 2023

@author: Siddharth
"""

import os
import cv2
from moviepy.editor import VideoFileClip
from file_data import File

class VideoData(File):
    
    def __init__(self, path):
        super().__init__(path)
        self.path = path
        self.__compute_aspect_ratio()
        self.__compute_video_length()
        self.__compute_bit_rate()
        self.update_duration()
        
    def __videoResolution(self):
        print(self.path)
        video = cv2.VideoCapture(self.path)
        _, frame = video.read()
        self.framerate = video.get(cv2.CAP_PROP_FPS)
        height, width, self.channels = frame.shape
        resolution = str(width) + 'x' + str(height)
        return resolution
       
    def __compute_aspect_ratio(self):
        width, height = self.__videoResolution().split('x')
        self.aspectratio = int(width)/int(height)

    def __compute_video_length(self):
        video = VideoFileClip(self.path)
        self.duration = video.duration
        video.close()

    def __compute_bit_rate(self):

        fileSize = os.path.getsize(self.path) / (1024 * 1024)
        self.bit_rate = fileSize * 8 / (self.duration) 

    def update_duration(self):
        hours = self.duration // 3600
        self.duration = self.duration % 3600
        minutes = self.duration // 60
        seconds = self.duration % 60

        if minutes < 10:
            minutes = f'0{str(minutes)}'

        if seconds < 10:
            seconds = f'0{str(seconds)}'
            
        self.duration = f'{str(hours)}:{minutes}:{seconds}'