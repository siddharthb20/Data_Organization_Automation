# -*- coding: utf-8 -*-
"""
Created on Sun May 7 09:23:31 2023

@author: Siddharth
"""

skipFiles = ["System Volume Information", "Program Files", "Program Files (x86)", \
             "ProgramData", "SWSetup", "Windows", "temp", "system.sav", "PerfLogs"\
                "minikube"]

IMAGES = ['.jpg', '.png', '.jpeg', '.bmp', '.svg', '.webp']

VIDEOS = ['.mp4', '.avi', '.mkv', '.3gp', '.flv', '.wmv']

AUDIOS = ['.mp3', '.m4a']

FIELD_TYPES = ['', 'text', 'int']