# -*- coding: utf-8 -*-
"""
Created on Mon May 15 09:29:59 2023

@author: Siddharth
"""

import os
import cv2
from file_data import File

class ImageData(File):
    
    def __init__(self, path):
        super().__init__(path)
        self.path = path
        self.channels = 0
        self.resolution = self.__imageResolution()
        self.aspectratio = self.compute_aspect_ratio()
        self.compression = 0
        self.transparency = 0
        
    def __imageResolution(self):
        img = cv2.imread(self.path)
        height, width, self.channels = img.shape
        resolution = str(width) + 'x' + str(height)
        return resolution
       
    def compute_aspect_ratio(self):
        width, height = self.resolution.split('x')
        aspectRatio = int(width)/int(height)
        return aspectRatio
    