import os
from file_data import File
from collections import OrderedDict
from tqdm import tqdm

def scan(folderList):

    folderList = createFolderList(folderList)
    fileList = createFileList(folderList)
    return folderList, fileList
    
def createFolderList(inputList):

    i = 0
    while i < len(inputList):
        
        item = inputList[i]
        i += 1
        try:
            subItems = os.listdir(item)
            itemPaths = [os.path.join(item, s) for s in subItems]
            folderPaths = [f for f in itemPaths if os.path.isdir(f)]
            inputList = inputList + folderPaths
            inputList = list(OrderedDict.fromkeys(inputList))
        except PermissionError:
            j = 0
    return inputList

def createFileList(folderList):
    fileList = []
    
    print("Discovering file items")
    for f in tqdm(folderList):        
        try:
            files = [os.path.join(f, file) for file in os.listdir(f)]
            fileList.extend(files)
        except PermissionError:
            j = 0

    fileList = [f for f in fileList if os.path.isfile(f)]
    return fileList



if __name__ == '__main__':
    path = ['D:\Git\Alternate\shibbydex_filter\src']
    scan(path)