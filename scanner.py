# -*- coding: utf-8 -*-
"""
Created on Sun May 7 08:12:31 2023

@author: Siddharth
"""

import os
import argparse
from file_data import File
from image_data import ImageData
import sys
from tqdm import tqdm
from constants import skipFiles, IMAGES
from utils import write2file, read_db, writeData2File

def parse_args():
    arg = argparse.ArgumentParser(description="Program to list all \
                                  files/folders in a location")
    
    # String arguments
    arg.add_argument("-p", '--path', help='Path to the folder (default C:)', \
                     action='append', nargs='+')
    arg.add_argument("-o", '--output', help='Path to requested location \
                     where the .csv or .db file must be stored')
        
    # Boolean arguments
    arg.add_argument("-r", '--read', help='Open in read-mode', action='store_true')
    arg.add_argument("-d", '--database', help='Use database mode. Program will \
                     read from/write to .db files', action='store_true')
    
    args = arg.parse_args()
    return args

# def scan(path, output, read=False, database=False):
def main():

    args = parse_args()
    
    # Set default location to C:
    path = list("'C:\'")
    if len(args.path[0])>0:
        path = args.path[0]
    
    print(args.path)

    # path = [path]
    filelist, folders, files = [], [], []

    # Check file type and read if SQL database
    if args.read and args.database:
        path = path
        ext = os.path.splitext(os.path.split(path[0][0])[1])[1]
        if ext.lower() != '.db':
            raise ValueError('Entered path is not a database.')
        database = path[0][0]
        read_db(database)
        return
        
    
    # Remove skipFiles and skipFolders and separate files from folders
    for p in path:
        filelist.extend([f for f in os.listdir(p) if '$' not in f])
        filelist = list(set(filelist)- set(skipFiles))
        filelist = sorted(filelist) 
        folders.extend([os.path.join(p, f) for f in filelist \
                        if os.path.isdir(os.path.join(p, f))])
        files.extend([os.path.join(p, f) for f in filelist \
                      if os.path.isfile(os.path.join(p, f))])

    # Populate list of files, recursively going through folders
    for f in tqdm(folders):
       fil, _ = folder_lists(f)
       files.extend(fil)

    # Convert list to File objects for easy access to attributes
    print(f'{len(files)} files found. Reading file data.')
    file_object = [File(f) for f in files]
    img_object = [ImageData(f.path) for f in file_object if f.ext in IMAGES]
    data = { 'generalFiles' : file_object,
            'imageFiles' : img_object}

    print('Completed reading data')
    
    # Write to either csv or create table database for SQL
    # write2file(file_object, to_db=True)
    if args.output:
        text = "CompiledData"#'\nYou chose to write data to file option. \nEnter desired name of output file. \nEntering no name will terminate the program without writing compiled data to file: '
        dataListName = text#input(text)
        #if not dataListName:
        #    return
        writeData2File(file_object, args.output, dataListName, to_db=args.database, to_csv=not(args.database))
    

def folder_lists(folder_path):
    """
    Function to recursively find all the files and folders in the passed 
    location.

    Parameters
    ----------
    folder_path : str
        Path to the base folder.

    Returns
    -------
    files : list
        List of paths of all the files in the folder.
    folders : list
        List of paths of all the folders in the folder.

    """
    folders, files = [], []
    for path, folder, file in os.walk(folder_path):
        fullPath = [os.path.join(path, f) for f in folder]
        folders.extend(fullPath)
        fullPath = [os.path.join(path, f) for f in file]
        files.extend(fullPath)
    return files, folders

if __name__=='__main__':
    main()